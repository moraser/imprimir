<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mike42\Escpos\Printer; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class ImprimirController extends Controller
{
    public function index(Request $request){         
                
        try {
            $connector = new WindowsPrintConnector("EPSON-TM-U675");
            //$connector = new FilePrintConnector("php://stdout");            
            $printer = new Printer($connector);
            //$printer -> text("Hello World!\n");
            $printer -> initialize();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("FRONTERA CELULAR\n");
            $printer->text("807009143-3\n");
            $printer->text("Calle 6 # 6-87 Centro - Telf. 5955758\n");
            $printer->text("CUCUTA - COLOMBIA\n");
            $printer->text("----------------------------------------\n");
            $printer->setJustification();
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $printer->text("Cajero(a): ".$request->cajero."\n");
            $printer->text("CVS: ".$request->sucursal."\n");
            $printer->text("Fecha: ".$request->fecha."\n");
            $printer->text("Hora: ".$request->hora."\n");
            $printer->text("Cod. Transaccion: ".$request->tx."\n");
            $printer->text("Concepto: ".$request->concepto."\n");
            $printer->text("Referencia: ".$request->referencia."\n");
            $printer->text("Valor: ".$request->valor."\n");
            $printer->setJustification();
            $printer->setEmphasis(false); 
            $printer->feed();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("ADVERTENCIA \n");
            $printer->text("Para cualquier solicitud o \n");
            $printer->text("reclamo es indispensable la\n");
            $printer->text("presentación de este \n");
            $printer->text("desprendible en OPTIMAS\n");
            $printer->text(" CONDICIONES. \n");
            $printer->feed(1);
            $printer->text("Los pagos realizados en ");
            $printer->text("días hábiles después de las ");
            $printer->text("07:00 p.m y en días feriados ");
            $printer->text("serán aplicados el día hábil ");
            $printer->text("siguiente. ");
            $printer->feed(3);
            $printer->cut();
            $printer->close();
            return ['respuesta' => 'Impresion exitosa'];
        } catch(Exception $e) {
            $estado = "Couldn't print to this printer: " . $e -> getMessage() . "\n";
            return ['respuesta' => $estado];
        }
    }
    public function store(Request $request){         
                
        try {
            $connector = new WindowsPrintConnector("EPSON-TM-U675");
            //$connector = new FilePrintConnector("php://stdout");            
            $printer = new Printer($connector);
            //$printer -> text("Hello World!\n");
            $printer -> initialize();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("FRONTERA CELULAR\n");
            $printer->text("807009143-3\n");
            $printer->text("Calle 6 # 6-87 Centro - Telf. 5955758\n");
            $printer->text("CUCUTA - COLOMBIA\n");
            $printer->text("----------------------------------------\n");
            $printer->setJustification();
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->selectPrintMode(Printer::MODE_EMPHASIZED);
            $printer->text("Cajero(a): ".$request->cajero."\n");
            $printer->text("CVS: ".$request->sucursal."\n");
            $printer->text("Fecha: ".$request->fecha."\n");
            $printer->text("Hora: ".$request->hora."\n");
            $printer->text("Cod. Transaccion: ".$request->tx."\n");
            $printer->text("Concepto: ".$request->concepto."\n");
            $printer->text("Referencia: ".$request->referencia."\n");
            $printer->text("Valor: ".$request->valor."\n");            
            $printer->setJustification();
            $printer->setEmphasis(false); 
            $printer->feed();
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text("ADVERTENCIA \n");
            $printer->text("Para cualquier solicitud o \n");
            $printer->text("reclamo es indispensable la\n");
            $printer->text("presentación de este \n");
            $printer->text("desprendible en OPTIMAS\n");
            $printer->text(" CONDICIONES. \n");
            $printer->feed(1);
            $printer->text("Los pagos realizados en ");
            $printer->text("días hábiles después de las ");
            $printer->text("07:00 p.m y en días feriados ");
            $printer->text("serán aplicados el día hábil ");
            $printer->text("siguiente. ");
            $printer->feed(3);
            $printer->cut();
            $printer->close();
            return ['respuesta' => 'Impresion exitosa'];
        } catch(Exception $e) {
            $estado = "Couldn't print to this printer: " . $e -> getMessage() . "\n";
            return ['respuesta' => $estado];
        }
    }

    
}
